package zadanietrzy.repo.domain;

import java.util.List;

public class Season extends Entity{
	
	private int seasonNumber;
	private int yearOfRelease;
	private String sezonName;
	private List<Episode> epizodes;
	
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}
	public String getSezonName() {
		return sezonName;
	}
	public void setSezonName(String sezonName) {
		this.sezonName = sezonName;
	}
	public List<Episode> getEpizodes() {
		return epizodes;
	}
	public void setEpizodes(List<Episode> epizodes) {
		this.epizodes = epizodes;
	}
	
	
	
}
