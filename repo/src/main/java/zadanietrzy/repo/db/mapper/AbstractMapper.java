package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import zadanietrzy.repo.domain.Entity;

public abstract class AbstractMapper <T extends Entity>{

	protected Connection connection;
	
	abstract protected String findStatement();
	abstract protected String insertStatement();
	abstract protected String updateStatement();
	abstract protected String removeStatement();
	
	abstract protected T doLoad(ResultSet rs) throws SQLException;
	abstract protected void parametrizeInsertStatement(PreparedStatement statement, T entity) throws SQLException;
	abstract protected void parametrizeUpdateStatement(PreparedStatement statement, T entity) throws SQLException;
	
	protected AbstractMapper(Connection connection) {
		this.connection = connection;
	}
	
	public T find(Long id) {
		T result = null;
		PreparedStatement findStatement = null;
		try {
			findStatement = connection.prepareStatement(findStatement());
			findStatement.setLong(1, id.longValue());
			ResultSet rs = findStatement.executeQuery();
			rs.next();
			result = load(rs);
			return result;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public void add(T entity) {
		PreparedStatement addStatement = null;
		try {
			addStatement = connection.prepareStatement(insertStatement());
			parametrizeInsertStatement(addStatement, entity);
			addStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public void update (T entity) {
		PreparedStatement updateStatement = null;
		try {
			updateStatement = connection.prepareStatement(updateStatement());
			parametrizeUpdateStatement(updateStatement, entity);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public void remove(Long id) {
		PreparedStatement removeStatement = null;
		try {
			removeStatement = connection.prepareStatement(removeStatement());
			removeStatement.setLong(1, id);
			removeStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	private T load(ResultSet rs) throws SQLException {
	
		T result = doLoad(rs);
		return result;
	}
}
