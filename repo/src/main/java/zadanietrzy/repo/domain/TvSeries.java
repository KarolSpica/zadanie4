package zadanietrzy.repo.domain;

import java.util.List;

public class TvSeries extends Entity{
	
	private String name;
	List<Season> seasons;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Season> getSeasons() {
		return seasons;
	}
	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}
	
	
	
	
}
