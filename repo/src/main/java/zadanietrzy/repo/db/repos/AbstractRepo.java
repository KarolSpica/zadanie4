package zadanietrzy.repo.db.repos;

import java.sql.Connection;

import zadanierzy.repo.db.Repository;
import zadanietrzy.repo.db.mapper.AbstractMapper;
import zadanietrzy.repo.db.uow.IUnitOfWorkRepository;
import zadanietrzy.repo.db.uow.UnitOfWork;
import zadanietrzy.repo.domain.Entity;

public abstract class AbstractRepo<TEntity extends Entity> implements Repository<TEntity>, IUnitOfWorkRepository{
	
	protected Connection connection;
	protected AbstractMapper<TEntity> mapper;
	protected UnitOfWork unitOfWork;
	
	
	public AbstractRepo(Connection connection, UnitOfWork unitOfWork) {
		this.connection = connection;
		this.unitOfWork = unitOfWork;
	}
	

	public void add(TEntity entity) {
		unitOfWork.markAsNew(entity, this);
		
	}


	public void modify(TEntity entity) {
		unitOfWork.markAsDirty(entity, this);
		
	}


	public void remove(TEntity entity) {
		unitOfWork.markAsDeleted(entity, this);
		
	}


	@SuppressWarnings("unchecked")
	public void persistAdd(Entity entity) {
		mapper.add((TEntity)entity);
		
	}


	@SuppressWarnings("unchecked")
	public void persistUpdate(Entity entity) {
		mapper.update((TEntity)entity);
		
	}


	public void persistDelete(Entity entity) {
		mapper.remove((long)entity.getId());
		
	}

	
	
	
	
}
