package zadanierzy.repo.db;

import zadanietrzy.repo.domain.Entity;

public interface Repository<TEntity extends Entity> {
	
	TEntity withId(int id);
	void add (TEntity entity);
	void modify (TEntity entity);
	void remove (TEntity entity);
}
