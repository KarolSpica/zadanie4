package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import zadanietrzy.repo.domain.Actor;

public class ActorMapper extends AbstractMapper<Actor>{
	
	private static final String COLUMNS = "id, name, birth,  biography";
	public static final String FIND_STMT = "SELECT " + COLUMNS + " FROM actor WHERE id=?";
	public static final String INSERT_STMT = "INSERT INTO actor(name, birth, biography) VALUES(?,?,?)";
	public static final String UPDATE_STMT = "UPDATE actor SET (name, birth,  biography)=(?,?,?) WHERE id=?";
	public static final String DELETE_STMT = "DELETE FROM actor WHERE id=?";
	
	public ActorMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_STMT;
	}

	@Override
	protected String insertStatement() {
		return INSERT_STMT;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_STMT;
	}

	@Override
	protected String removeStatement() {
		return DELETE_STMT;
	}

	@Override
	protected Actor doLoad(ResultSet rs) throws SQLException {
		Actor actor = new Actor();
		actor.setName(rs.getString("name"));
		actor.setDateOfBirth(rs.getDate("birth").toLocalDate());
		actor.setBiography(rs.getString("biography"));
		actor.setId(rs.getInt("id"));
		return actor;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Actor actor) throws SQLException {
		statement.setString(1, actor.getName());
		statement.setDate(2,Date.valueOf(actor.getDateOfBirth()));
		statement.setString(3, actor.getBiography());
		
		
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Actor actor) throws SQLException {
		parametrizeInsertStatement(statement, actor);
		statement.setLong(4, actor.getId());
		
	}
}
