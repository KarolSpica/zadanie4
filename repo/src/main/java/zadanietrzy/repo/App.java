package zadanietrzy.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zadanietrzy.repo.db.catalogs.HsqlRepositoryCatalog;
import zadanietrzy.repo.db.uow.IUnitOfWorkRepository;
import zadanietrzy.repo.db.uow.UnitOfWork;
import zadanietrzy.repo.domain.Actor;
import zadanietrzy.repo.domain.Director;
import zadanietrzy.repo.domain.Entity;
import zadanietrzy.repo.domain.Episode;
import zadanietrzy.repo.domain.Season;
import zadanietrzy.repo.domain.TvSeries;


public class App 
{
    public static void main( String[] args )
    {
        String url = "jdbc:hsqldb:hsql://localhost/testdb";
        Connection connection;
        try{
        	
        	connection = DriverManager.getConnection(url);
        	UnitOfWork uow = new UnitOfWork(connection);
        	HsqlRepositoryCatalog catalog = new HsqlRepositoryCatalog(connection, uow);
        	
        	LocalDate data = LocalDate.of(1989, 6, 22);
        	/*
        	Actor aktor = new Actor();
        	aktor.setBiography("biografia");
        	aktor.setDateOfBirth(data);
        	aktor.setName("Karol");
        	aktor.setId(1);
       
        	
        	catalog.actor().persistDelete(aktor);                               
        	uow.commit();
     	
        	*/
        	Episode epizod = new Episode();
        	epizod.setDuration(12);
        	epizod.setName("nazwa");
        	epizod.setReleaseDate(data);
        	epizod.setEpisodeNumber(1);
        	epizod.setSeasonName("Dexter");
        	
        	Episode epizod1 = new Episode();
        	epizod1.setDuration(12);
        	epizod1.setName("nowanazwa");
        	epizod1.setReleaseDate(data);
        	epizod1.setEpisodeNumber(1);
        	epizod1.setSeasonName("Dexter");
        	
        	List<Episode> epizody = new ArrayList<Episode>();
        	epizody.add(epizod);
        	epizody.add(epizod1);
        	
        
        	
        	Season sezon = new Season();
            sezon.setEpizodes(null);
        	sezon.setSeasonNumber(2);
        	sezon.setYearOfRelease(1999);
            sezon.setSezonName("Dexter");
            sezon.setEpizodes(epizody);
            
            List<Season> sezony = new ArrayList<Season>();
        	sezony.add(sezon);
        	
            TvSeries tvSeries = new TvSeries();
            tvSeries.setName("Dexter");
            tvSeries.setSeasons(sezony);
            
            catalog.tvseries().add(tvSeries);
            uow.commit();
        	
        	/*
        	TvSeries tvSeries = new TvSeries();
        	tvSeries.setName("Dexter");
        	tvSeries.setSeasons(sezony);
        	*/
        	;
       
        } catch (SQLException e) {
        	e.printStackTrace();
        }
    }
}
