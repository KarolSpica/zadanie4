package zadanietrzy.repo.db.repos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public  class ChceckTable {

public static void checkIfTableExsist(Connection connection, String tableName, String createTable) {
	try {
		ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		boolean tableExsist = false;
		
		while(rs.next()) {
			if(rs.getString("TABLE_NAME").equalsIgnoreCase(tableName)) {
				tableExsist = true;
				break;
			}
		}
		if(!tableExsist) {
			Statement createTableStatement = connection.createStatement();
			createTableStatement.executeUpdate(createTable);
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
}
}