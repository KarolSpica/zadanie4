package zadanietrzy.repo.domain;

public enum EntityState {
	New, Changed, Unchanged, Deleted
}
