package zadanietrzy.repo.domain;

import java.time.LocalDate;

public class Actor extends Entity{
	
	private String name;
	private String biography;
	private LocalDate dateOfBirth;
	
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	@Override
	public String toString() {
		return "Actor [name=" + name + ", biography=" + biography + ", dateOfBirth=" + dateOfBirth + "]";
	}
	
	
}
