package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import zadanietrzy.repo.domain.Director;

public class DirectorMapper extends AbstractMapper<Director>{
	
	private static final String COLUMNS = "id, name, birth,  biography";
	public static final String FIND_STMT = "SELECT " + COLUMNS + " FROM director WHERE id=?";
	public static final String INSERT_STMT = "INSERT INTO director(name, birth, biography) VALUES(?,?,?)";
	public static final String UPDATE_STMT = "UPDATE director SET (name, birth,  biography)=(?,?,?) WHERE id=?";
	public static final String DELETE_STMT = "DELETE FROM director WHERE id=?";
	
	public DirectorMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_STMT;
	}

	@Override
	protected String insertStatement() {
		return INSERT_STMT;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_STMT;
	}

	@Override
	protected String removeStatement() {
		return DELETE_STMT;
	}

	@Override
	protected Director doLoad(ResultSet rs) throws SQLException {
		Director director = new Director();
		director.setName(rs.getString("name"));
		director.setDateOfBirth(rs.getDate("birth").toLocalDate());
		director.setBiography(rs.getString("biography"));
		director.setId(rs.getInt("id"));
		return director;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Director director) throws SQLException {
		statement.setString(1, director.getName());
		statement.setDate(2, Date.valueOf(director.getDateOfBirth()));
		statement.setString(3, director.getBiography());
		
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Director director) throws SQLException {
		parametrizeInsertStatement(statement, director);
		statement.setLong(4, director.getId());
		
	}
	
	
}
