package zadanietrzy.repo.db.catalogs;

import java.sql.Connection;

import zadanietrzy.repo.db.repos.HsqlActorRepository;
import zadanietrzy.repo.db.repos.HsqlDirectorRepository;
import zadanietrzy.repo.db.repos.HsqlEpisodeRepository;
import zadanietrzy.repo.db.repos.HsqlSeasonRepository;
import zadanietrzy.repo.db.repos.HsqlTvSeriesRepository;
import zadanietrzy.repo.db.uow.UnitOfWork;

public class HsqlRepositoryCatalog {
	
	private Connection connection;
	private UnitOfWork unitOfWork;
	private HsqlActorRepository actorRepository;
	private HsqlDirectorRepository directorRepository;
	private HsqlTvSeriesRepository tvSeriesRepository;
	private HsqlSeasonRepository seasonRepository;
	private HsqlEpisodeRepository episodeRepository;
	
	public HsqlRepositoryCatalog (Connection connection, UnitOfWork unitOfWork) {
		this.connection = connection;
		this.unitOfWork = unitOfWork;
		actorRepository = new HsqlActorRepository(connection, unitOfWork);
		directorRepository = new HsqlDirectorRepository(connection, unitOfWork);
		tvSeriesRepository = new HsqlTvSeriesRepository(connection, unitOfWork);
		seasonRepository = new HsqlSeasonRepository(connection, unitOfWork);
		episodeRepository = new HsqlEpisodeRepository(connection, unitOfWork);
		
	}

	public HsqlActorRepository actor() {
		return actorRepository;
	}
	public HsqlDirectorRepository director() {
		return directorRepository;
	}
	public HsqlTvSeriesRepository tvseries() {
		return tvSeriesRepository;
	}

	public HsqlSeasonRepository season() {
		return seasonRepository;
	}

	public HsqlEpisodeRepository episode() {
		return episodeRepository;
	}
	

}
