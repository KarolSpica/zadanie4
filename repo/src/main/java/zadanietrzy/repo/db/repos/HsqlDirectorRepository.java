package zadanietrzy.repo.db.repos;

import java.sql.Connection;
import zadanierzy.repo.db.Repository;
import zadanietrzy.repo.db.mapper.DirectorMapper;
import zadanietrzy.repo.db.uow.UnitOfWork;
import zadanietrzy.repo.domain.Director;

public class HsqlDirectorRepository extends AbstractRepo<Director> implements Repository<Director>{
	
	private String tableName = "Director";
	private String createDirectorTable = ""
			+ "CREATE TABLE Director("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "name VARCHAR(20),"
			+ "birth DATE,"
			+ "biography VARCHAR(80)"
			+ ")";
	
	public HsqlDirectorRepository(Connection connection, UnitOfWork unitOfWork) {
		super(connection,unitOfWork);
		mapper = new DirectorMapper(connection);
		ChceckTable.checkIfTableExsist(connection, tableName, createDirectorTable);
	}

	public Director withId(int id) {
		return mapper.find((long)id);
	}
	
	
}
